module.exports = {
  extend: 'apostrophe-pieces',
  label: 'Destino',
  name: 'destinos',
  alias: 'destinos',
  // contextualOnly: true,
  addFields: [









  {
      name: 'imagen',
      label: 'Insertar la imagen',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      required:true
      // textarea: true
      
    },
    {
      name: 'nombreDestino',
      label: 'Insertar el Nombre del Destino',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      required:true
      // textarea: true
      
    },
    {
      name: 'precio',
      label: 'Inserte el precio',
      help: 'Porfavor, inserte el simbolo de Peso acompañado del precio ',
      type: 'string',
      // textarea: true
      
    },
    {
      name: 'location',
      label: 'Inserte la dirección',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      textarea: true
      
    },
    {
      name: 'bedrooms',
      label: 'Insertar cantidad de cuartos',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      // textarea: true
      
    },
    {
      name: 'bathrooms',
      label: 'Insertar cantidad de baños',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      // textarea: true
      
    },
    {
      name: 'Lugar',
      label: 'Insertar lugar',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      textarea: true
      
    },
    {
      name: 'garage',
      label: 'Insertar cantidad de garajes',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      // textarea: true
      
    },
    {
      name: 'user',
      label: 'Insertar usuario',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      // textarea: true
      
    },
    {
      name: 'calendar',
      label: 'Insertar tiempo',
      // help: 'Inserte el Tipo de Elemento html a insertar en el documento',
      type: 'string'
      // textarea: true
      
    }
    
  ],arrangeFields: [
    {
      name: 'dest',
      label: 'Destino',
      fields: [ 'nombreDestino', 'precio', 'location', 'bedrooms', 'bathrooms', 'Lugar', 'garage', 'user', 'calendar' ]
    }
  ],construct: function(self, options) {
    self.beforeSave = function(req, piece, options, callback) {
      piece.title = piece.nombreDestino ;
      piece.slug = piece.nombreDestino ;
      return callback();
    };
  }
};

