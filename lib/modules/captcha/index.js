module.exports = {
    // extend: 'apostrophe-pieces',
    name: 'captcha',
    label: 'captcha',
    alias: 'captcha',
    //Estos campos por ahora se estan ignorando
    //la collection acepta cualquier cosa
    
    construct: function (self, options) {
        
        self.apos.app.get('/captcha', function (req, res) {
          var svgCaptcha = require('svg-captcha');
 var captcha = svgCaptcha.create({
    color: true,
    fontSize:80,
    width:200,
    heigth:120
});
    req.session.captcha = captcha.text;
    


    res.type('svg');
    res.status(200).send(captcha.data);

                
        });
    }
}