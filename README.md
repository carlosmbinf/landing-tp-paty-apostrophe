## Building docker image
You will need the following things properly installed on your computer.

* [Docker](http://www.docker.com/)

Now that we have a Dockerfile, it's time to build an image for your project. You can do this with docker-compose build.

```bash
$ cd ~/Projects/landing-tp-paty-apostrophe
$ docker-compose build
```
## Docker Swarm
We use docker secrets to send sensitive information to container. For that reason we need to run docker engine in swarm mode. Swarm is the cluster manager integrated with docker engine since 1.12.0 version. To init docker in swarm mode use this:

```bash
$ docker swarm init
```
## Run containers with docker-compose
To run containers with docker-compose:
```bash
$ docker-compose up -d
```
To stop the docker-compose services:
```bash
$ docker-compose down
```
## Deploy docker stacks
To deploy the containers with docker in swarm mode we can use docker stack. Stacks allow for multiple services, which are containers distributed across a swarm, to be deployed and grouped logically. To get a stack up and running just requires a docker-compose file.

## Deploy besthings stack
We're almost ready to deploy a new stack called besthings

```bash
$ docker stack deploy -c docker-compose.yml besthings
```
Check that your services are deployed:
```bash
$ docker service ls
```
this is the output:
```bash
ID              NAME                     MODE          REPLICAS    IMAGE                 PORTS
xpsqdnzc0zar    landyngtppaty   replicated    1/1         landyngtppaty:latest   *:3000->3000/tcp
dql5axsdexcs    mongo          replicated    1/1         mongo:latest          *:27017->27017/tcp
```
